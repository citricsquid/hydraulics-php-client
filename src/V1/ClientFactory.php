<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1;

use Http\Client\HttpClient;
use Http\Message\MessageFactory as MessageFactoryInterface;
use Hydrawiki\Hydraulics\Client\V1\Resources\Configuration;
use Hydrawiki\Hydraulics\Client\V1\Resources\Environment;
use Hydrawiki\Hydraulics\Client\V1\Resources\Setting;
use Hydrawiki\Hydraulics\Client\V1\Resources\Wiki;

class ClientFactory
{
    /**
     * Resource types and their companion Resource.
     *
     * @var \Hydrawiki\Hydraulics\Client\V1\Resource[]
     */
    protected $resources = [
        'configurations' => Configuration::class,
        'environments'   => Environment::class,
        'settings'       => Setting::class,
        'wikis'          => Wiki::class,
    ];

    /**
     * Make a new Hydraulics Client using the HTTP Client, message factory and
     * endpoint provided by the package user.
     *
     * @param \Http\Client\HttpClient      $http
     * @param \Http\Message\MessageFactory $messageFactory
     * @param string                       $endpoint
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Client
     */
    public function make(
        HttpClient $http,
        MessageFactoryInterface $messageFactory,
        string $endpoint
    ): Client {
        $messageFactory = new MessageFactory($messageFactory, $endpoint);
        $api = new Api($http, $messageFactory);

        $resourceFactory = new ResourceFactory($this->resources);

        $hydrator = new Hydrator($resourceFactory);

        return new Client($api, $hydrator);
    }
}
