<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1\Exceptions;

use Hydrawiki\Hydraulics\Client\V1\Resource;
use LogicException;

class ResourceRelationshipUndefined extends LogicException
{
    /**
     * The relationship has not been defined as part of the Resource.
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\Resource $resource
     * @param string                                   $relationship
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Exceptions\ResourceRelationshipUndefined
     */
    public static function relationship(Resource $resource, string $relationship): self
    {
        return new static("Relationship {$relationship} does not exist on Resource ".get_class($resource));
    }
}
