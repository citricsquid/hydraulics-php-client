<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1\Resources;

use Hydrawiki\Hydraulics\Client\V1\Resource;

class Setting extends Resource
{
    /**
     * Resource type as per the API.
     *
     * @var string
     */
    protected $type = 'settings';

    /**
     * Attributes provided by the API and default values.
     *
     * @var array
     */
    protected $attributes = [
        'is-protected'  => null,
        'is-restricted' => null,
        'value'         => null,
    ];

    /**
     * Relationships to other Resources.
     *
     * @var array
     */
    protected $relationships = [
        'configuration' => [Configuration::class, self::RELATIONSHIP_ONE],
        'wiki'          => [Wiki::class, self::RELATIONSHIP_ONE],
    ];
}
