<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1\Exceptions;

use Hydrawiki\Hydraulics\Client\V1\Resource;
use LogicException;

class ResourceAlreadyPopulated extends LogicException
{
    /**
     * The Resource has already been populated with attributes.
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\Resource $resource
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Exceptions\ResourceAlreadyPopulated
     */
    public static function resource(Resource $resource): self
    {
        return new static('Resource has already been populated, use `update()` to make changes.');
    }
}
