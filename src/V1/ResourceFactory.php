<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1;

use Hydrawiki\Hydraulics\Client\V1\Exceptions\ResourceTypeUnmapped;

class ResourceFactory
{
    /**
     * Resource Object type to Resource.
     *
     * @var array
     */
    protected $resourceMap;

    /**
     * Map of Resource Object types (from the API) to local Resources.
     *
     * @param array $resourceMap
     */
    public function __construct(array $resourceMap)
    {
        $this->resourceMap = $resourceMap;
    }

    /**
     * Make a Resource from a ResourceObject.
     *
     * @param string $type
     *
     * @throws \Hydrawiki\Hydraulics\Client\V1\Exceptions\ResourceTypeUnmapped
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Resource
     */
    public function make(string $type): Resource
    {
        if (!array_key_exists($type, $this->resourceMap)) {
            throw ResourceTypeUnmapped::type($type);
        }

        return new $this->resourceMap[$type]();
    }
}
