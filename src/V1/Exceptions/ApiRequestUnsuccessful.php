<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1\Exceptions;

use Hydrawiki\Hydraulics\Client\V1\JsonApiResponse;
use Hydrawiki\Hydraulics\Client\V1\Resource;
use LogicException;

class ApiRequestUnsuccessful extends LogicException
{
    /**
     * API Response was not successful for an Index request.
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\JsonApiResponse $response
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Exceptions\ApiResponseFailed
     */
    public static function index(JsonApiResponse $response): self
    {
        return new static('Index resource request to API was not successful.');
    }

    /**
     * API Response was not successful for a Read request.
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\JsonApiResponse $response
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Exceptions\ApiResponseFailed
     */
    public static function read(JsonApiResponse $response): self
    {
        return new static('Read resource request to API was not successful.');
    }
}
