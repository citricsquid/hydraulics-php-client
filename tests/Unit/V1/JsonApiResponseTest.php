<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\Tests\Unit\V1;

use Hydrawiki\Hydraulics\Client\V1\Document;
use Hydrawiki\Hydraulics\Client\V1\JsonApiResponse;
use PHPUnit\Framework\TestCase;

class JsonApiResponseTest extends TestCase
{
    /**
     * Tests that an index is successful if the status code is 200 and the
     * document contains many primary resources.
     */
    public function testIsSuccessfulIndex(): void
    {
        $document = $this->createMock(Document::class);
        $document->method('isMany')->willReturn(true);

        $response = new JsonApiResponse(200, $document);
        $this->assertTrue($response->isSuccessfulIndex());
    }

    /**
     * Tests that a read is successful if the status codei s 200 and the
     * document has one primary resource.
     */
    public function testIsSuccessfulRead(): void
    {
        $document = $this->createMock(Document::class);
        $document->method('isOne')->willReturn(true);

        $response = new JsonApiResponse(200, $document);
        $this->assertTrue($response->isSuccessfulRead());
    }
}
