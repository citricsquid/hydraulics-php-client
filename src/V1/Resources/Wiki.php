<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1\Resources;

use Hydrawiki\Hydraulics\Client\V1\Resource;

class Wiki extends Resource
{
    /**
     * Resource type as per the API.
     *
     * @var string
     */
    protected $type = 'wikis';

    /**
     * Attributes provided by the API and default values.
     *
     * @var array
     */
    protected $attributes = [
        'created-at'        => null,
        'database-name'     => null,
        'database-password' => null,
        'database-username' => null,
        'hostname'          => null,
        'is-available'      => null,
        'name'              => null,
        'updated-at'        => null,
    ];

    /**
     * Relationships to other Resources.
     *
     * @var array
     */
    protected $relationships = [
        'environment' => [Environment::class, self::RELATIONSHIP_ONE],
        'parent'      => [self::class, self::RELATIONSHIP_ONE],
        'settings'    => [Setting::class, self::RELATIONSHIP_MANY],
    ];
}
