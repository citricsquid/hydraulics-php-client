<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1;

class JsonApiResponse
{
    /**
     * Response HTTP Status Code.
     *
     * @var int
     */
    protected $statusCode;

    /**
     * Optional Document included in the Response.
     *
     * @var \Hydrawiki\Hydraulics\Client\V1\Document|null
     */
    protected $document;

    /**
     * Constructs a new JSON API Response.
     *
     * @param int                                           $statusCode
     * @param \Hydrawiki\Hydraulics\Client\V1\Document|null $document
     */
    public function __construct(int $statusCode, ?Document $document = null)
    {
        $this->statusCode = $statusCode;
        $this->document = $document;
    }

    /**
     * Get the response Document.
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Document|null
     */
    public function document(): ?Document
    {
        return $this->document;
    }

    /**
     * Is this a successful response to an Index request?
     *
     * @return bool
     */
    public function isSuccessfulIndex(): bool
    {
        return $this->isStatusCode([200]) && $this->document() && $this->document()->isMany();
    }

    /**
     * Is this a successful response to a Read request?
     *
     * @return bool
     */
    public function isSuccessfulRead(): bool
    {
        return $this->isStatusCode([200]) && $this->document() && $this->document()->isOne();
    }

    /**
     * Get the HTTP Status Code of the response.
     *
     * @return int
     */
    public function statusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Does the HTTP Status Code match any of these status codes?
     *
     * @param array $statusCodes
     *
     * @return bool
     */
    protected function isStatusCode(array $statusCodes): bool
    {
        return in_array($this->statusCode, $statusCodes);
    }
}
