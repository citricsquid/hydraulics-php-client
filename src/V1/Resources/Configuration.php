<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1\Resources;

use Hydrawiki\Hydraulics\Client\V1\Resource;

class Configuration extends Resource
{
    /**
     * Resource type as per the API.
     *
     * @var string
     */
    protected $type = 'configurations';

    /**
     * Attributes provided by the API and default values.
     *
     * @var array
     */
    protected $attributes = [
        'datatype'    => null,
        'default'     => null,
        'description' => null,
        'key'         => null,
        'name'        => null,
    ];

    /**
     * Relationships to other Resources.
     *
     * @var array
     */
    protected $relationships = [
        'children' => [self::class, self::RELATIONSHIP_MANY],
        'parent'   => [self::class, self::RELATIONSHIP_ONE],
    ];
}
