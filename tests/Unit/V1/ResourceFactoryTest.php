<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\Tests\Unit\V1;

use Hydrawiki\Hydraulics\Client\V1\Exceptions\ResourceTypeUnmapped;
use Hydrawiki\Hydraulics\Client\V1\Resource;
use Hydrawiki\Hydraulics\Client\V1\ResourceFactory;
use PHPUnit\Framework\TestCase;

class ResourceFactoryTest extends TestCase
{
    /**
     * Tests that a Resource is created from a resource type.
     */
    public function testResourceIsMadeFromType(): void
    {
        $resourceType = new class() extends Resource {
        };

        $factory = new ResourceFactory([
            'examples' => get_class($resourceType),
        ]);

        $resource = $factory->make('examples');

        $this->assertInstanceOf(get_class($resourceType), $resource);
    }

    /**
     * Tests that when a resource type has not been mapped to a Resource that an
     * exception is thrown.
     */
    public function testUnmappedResourceTypeThrowsException(): void
    {
        $factory = new ResourceFactory([]);

        $this->expectException(ResourceTypeUnmapped::class);

        $factory->make('undefined');
    }
}
