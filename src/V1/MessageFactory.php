<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1;

use Http\Message\MessageFactory as HttpMessageFactory;
use Psr\Http\Message\RequestInterface;
use WoohooLabs\Yang\JsonApi\Schema\Document as YangDocument;

class MessageFactory
{
    /**
     * HTTP Message Factory.
     *
     * @var \Http\Message\MessageFactory
     */
    protected $httpMessageFactory;

    /**
     * API Endpoint for all requests.
     *
     * @var string
     */
    protected $endpoint;

    /**
     * Headers included with the request.
     *
     * @var array
     */
    protected $headers;

    /**
     * Constructs a new Message Factory.
     *
     * @param \Http\Message\MessageFactory $httpMessageFactory
     * @param string                       $endpoint
     * @param array                        $headers
     */
    public function __construct(
        HttpMessageFactory $httpMessageFactory,
        string $endpoint,
        array $headers = []
    ) {
        $this->httpMessageFactory = $httpMessageFactory;
        $this->endpoint = $endpoint;
        $this->headers = array_merge([
            'Accept'       => 'application/vnd.api+json',
            'Content-Type' => 'application/vnd.api+json',
        ], $headers);
    }

    /**
     * Create a request through the Message Factory, using the headers and
     * API endpoint.
     *
     * @param string      $method
     * @param string      $path
     * @param string|null $body
     *
     * @return \Psr\Http\Message\RequestInterface
     */
    public function createRequest(
        string $method,
        string $path,
        ?string $body = null
    ): RequestInterface {
        return $this->httpMessageFactory->createRequest(
            $method,
            "{$this->endpoint}/{$path}",
            $this->headers,
            $body
        );
    }

    /**
     * Create a JsonApiResponse from a Response body.
     *
     * @param int         $statusCode
     * @param string|null $document
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\JsonApiResponse
     */
    public function createResponse(int $statusCode = 200, ?string $document = null): JsonApiResponse
    {
        if (is_string($document)) {
            $document = new Document(
                YangDocument::fromArray(json_decode($document, true))
            );
        }

        return new JsonApiResponse($statusCode, $document);
    }
}
